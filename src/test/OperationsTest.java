import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import operations.AddOperation;
import operations.DivideOperation;
import operations.MultiplyOperation;
import operations.SubtractOperation;

import org.junit.Before;
import org.junit.Test;

public class OperationsTest {
	private BigDecimal first;
	private BigDecimal second;

	public AddOperation addOperation = new AddOperation();
	public SubtractOperation susbtractOperation = new SubtractOperation();
	public MultiplyOperation multiplyOperation = new MultiplyOperation();
	public DivideOperation divideOperation = new DivideOperation();

	@Before
	public void init() {
		first = new BigDecimal(10);
		second = new BigDecimal(10);
	}

	@Test
	public void testAddition() {
		BigDecimal value = first.add(second);
		assertEquals(value, addOperation.apply(first, second));
	}

	@Test
	public void testAdditionToString() {
		assertEquals("+", addOperation.toString());
	}

	@Test
	public void testSubtraction() {
		BigDecimal value = first.subtract(second);
		assertEquals(value, susbtractOperation.apply(first, second));
	}

	@Test
	public void testSubtractionToString() {
		assertEquals("-", susbtractOperation.toString());
	}

	@Test
	public void testMultiplication() {
		BigDecimal value = first.multiply(second);
		assertEquals(value, multiplyOperation.apply(first, second));
	}

	@Test
	public void testMultiplicationToString() {
		assertEquals("*", multiplyOperation.toString());
	}

	@Test
	public void testDivision() {
		BigDecimal value = first.divide(second);
		assertEquals(value, divideOperation.apply(first, second));
	}

	@Test
	public void testDivisionToString() {
		assertEquals("/", divideOperation.toString());
	}
}
