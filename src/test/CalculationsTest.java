import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import operations.AddOperation;
import operations.BinaryOperation;
import operations.DivideOperation;
import operations.MultiplyOperation;
import operations.SubtractOperation;

import org.junit.Before;
import org.junit.Test;

public class CalculationsTest {

	Calculator calculator = new Calculator();
	BigDecimal first;
	BigDecimal second;

	@Before
	public void setTestValues() {
		first = new BigDecimal(500.1);
		second = new BigDecimal(12.2);
		calculator.setFirst(first);
		calculator.setSecond(second);
	}

	@Test
	public void testAddition() {
		BigDecimal value = first.add(second);
		calculator.setOperation(new AddOperation());
		assertEquals(value, calculator.performCalcuation());
	}

	@Test
	public void testSubtraction() {
		BigDecimal value = first.subtract(second);
		calculator.setOperation(new SubtractOperation());
		assertEquals(value, calculator.performCalcuation());
	}

	@Test
	public void testMultiplication() {
		BigDecimal value = first.multiply(second);
		calculator.setOperation(new MultiplyOperation());
		assertEquals(value, calculator.performCalcuation());
	}

	@Test
	public void testDivision() {
		BigDecimal value = first.divide(second, BinaryOperation.DECIMAL_PLACES);
		calculator.setOperation(new DivideOperation());
		assertEquals(value, calculator.performCalcuation());
	}

}