import java.math.BigDecimal;

import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
	Calculator calculator = new Calculator();

	@Test
	public void createNewCalculatorWithZeroInitialValues() {
		assertEquals(BigDecimal.ZERO, calculator.getFirst());
		assertEquals(BigDecimal.ZERO, calculator.getSecond());
	}

	@Test
	public void testSetFirst() {
		BigDecimal value = (BigDecimal.ZERO);
		calculator.setFirst(value);
		assertEquals(value, calculator.getFirst());
	}

	@Test
	public void testSetSecond() {
		BigDecimal value = (BigDecimal.ZERO);
		calculator.setSecond(value);
		assertEquals(value, calculator.getSecond());
	}

	@Test
	public void testInitialOperatorIsNull() {
		assertEquals(null, calculator.getOperation());
	}

}