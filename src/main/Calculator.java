import java.math.BigDecimal;

import operations.BinaryOperation;

public class Calculator {

	private BigDecimal first = BigDecimal.ZERO;
	private BigDecimal second = BigDecimal.ZERO;

	private BinaryOperation operation;

	public BigDecimal getFirst() {
		return first;
	}

	public BigDecimal getSecond() {
		return second;
	}

	public void setFirst(BigDecimal value) {
		first = value;

	}

	public void setSecond(BigDecimal value) {
		second = value;

	}

	public BigDecimal performCalcuation() {
		return operation.apply(first, second);
	}

	public BinaryOperation getOperation() {
		return operation;
	}

	public void setOperation(BinaryOperation operation) {
		this.operation = operation;
	}

}