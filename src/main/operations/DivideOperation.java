package operations;

import java.math.BigDecimal;

public class DivideOperation extends BinaryOperation {

	public DivideOperation() {
		super("/");
	}

	@Override
	public BigDecimal apply(BigDecimal first, BigDecimal second) {
		return first.divide(second, BinaryOperation.DECIMAL_PLACES);
	}

}
