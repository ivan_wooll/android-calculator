package operations;

import java.util.Arrays;

public abstract class BinaryOperation implements OperationPerformer {

	public static final int DECIMAL_PLACES = 4;
	private String[] operands = { "-", "+", "*", "/" };
	protected String type;

	public BinaryOperation(String type) {
		if (!isValidOperation(type)) {
			throw new IllegalStateException("only the characters "
					+ String.valueOf(operands) + " allowed");
		}
		this.type = type;
	}

	@Override
	public String toString() {
		return type;
	}

	private boolean isValidOperation(String s) {
		return Arrays.asList(operands).contains(s);
	}
}
