package operations;
import java.math.BigDecimal;

public class SubtractOperation extends BinaryOperation {

	public SubtractOperation() {
		super("-");
	}

	@Override
	public BigDecimal apply(BigDecimal first, BigDecimal second) {
		return first.subtract(second);
	}

}
