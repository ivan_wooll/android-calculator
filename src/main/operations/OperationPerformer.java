package operations;
import java.math.BigDecimal;

public interface OperationPerformer {

	public BigDecimal apply(BigDecimal first, BigDecimal second);

}
