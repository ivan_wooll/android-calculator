package operations;
import java.math.BigDecimal;

public class AddOperation extends BinaryOperation {

	public AddOperation() {
		super("+");
	}

	@Override
	public BigDecimal apply(BigDecimal first, BigDecimal second) {
		return first.add(second);
	}

}
