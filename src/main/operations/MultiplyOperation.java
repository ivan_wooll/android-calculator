package operations;
import java.math.BigDecimal;

public class MultiplyOperation extends BinaryOperation {

	public MultiplyOperation() {
		super("*");
	}

	@Override
	public BigDecimal apply(BigDecimal first, BigDecimal second) {
		return first.multiply(second);
	}

}
